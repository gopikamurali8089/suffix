#python program to find the longest common suffix in a string 
def suffix(strings):
    if not strings:
        return ""
    shortest=min(strings,key=len)
    count=0
    for i in range(len(shortest)):
        for j in strings:
          if j[-i-1]!=shortest[-i-1]:
            return shortest[-i:] if i>0 else " "
    return shortest
number=int(input("enter the number of strings"))   
strings=[]
for i in range(number):
    user_input=input("enter the string")
    strings.append(user_input)
print("Longest common suffix:",suffix(strings))